FROM openjdk:8
RUN rm -rf app/
RUN mkdir app
ADD target/pickaname.jar app/pickaname.jar
EXPOSE 7777
RUN  echo '{"Candidate With Status":[{"Matt":"0","Ely":"0","Venky":"0","Rashmi":"0","Sri":"1","Bash":"1","Jeevan":"0","Nizar":"0"}],"Author":"Jeevan Abraham","Name":"Pick a name datacentre with status"}'> /app/db.json
ENTRYPOINT ["java","-jar","app/pickaname.jar"]



