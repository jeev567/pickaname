package com.pickaname;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PickanameApplication {

	public static void main(String[] args) {
		SpringApplication.run(PickanameApplication.class, args);
	}

}
