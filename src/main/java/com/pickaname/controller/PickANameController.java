package com.pickaname.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pickaname.serv.PickServ;


@RestController
@RequestMapping("/v1")
public class PickANameController {

	@Autowired
	private PickServ pickServ;

	@GetMapping(value = "/name")
	@ResponseBody
	public String getResources() throws Exception {
		return pickServ.pickAName();
	}
}
