package com.pickaname.serv;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import org.json.simple.parser.ParseException;

public interface JsonServ {
	void write(String currentName) throws FileNotFoundException, IOException, ParseException;
	Map<String, String> read() throws FileNotFoundException, IOException, ParseException;
	void reset() throws IOException;
}
