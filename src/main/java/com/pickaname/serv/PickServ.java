package com.pickaname.serv;

import java.io.IOException;

import org.json.simple.parser.ParseException;

public interface PickServ {

	public String pickAName() throws IOException, ParseException;

}
