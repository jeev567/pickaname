package com.pickaname.serv.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.pickaname.serv.JsonServ;
import com.pickaname.serv.PickServ;

@Service
public class PcikServImpl implements PickServ {

	@Value("#{'${res.candidate}'.split(',')}")
	private List<String> givenList;

	@Autowired
	private JsonServ jsonServ;

	@Override
	public String pickAName() throws IOException, ParseException {

		Map<String, String> candidates = jsonServ.read();
		final List<String> leftCandidates = new ArrayList<String>();
		formList(candidates, leftCandidates);
		if (leftCandidates.size() == 0) {
			jsonServ.reset();
			candidates = jsonServ.read();
			formList(candidates, leftCandidates);
		}
		final Random rand = new Random();
		final int randomIndex = rand.nextInt(leftCandidates.size());
		jsonServ.write(leftCandidates.get(randomIndex));

		return leftCandidates.get(randomIndex);
	}

	private void formList(final Map<String, String> candidates, final List<String> leftCandidates) {
		for (final Map.Entry<String, String> entry : candidates.entrySet()) {
			if (!entry.getValue().equals("1")) {
				System.out.println(entry.getKey());
				leftCandidates.add(entry.getKey());
			}
		}
	}

}
