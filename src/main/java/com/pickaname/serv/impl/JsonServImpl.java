package com.pickaname.serv.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import com.pickaname.serv.JsonServ;

@Service
public class JsonServImpl implements JsonServ {

	@Value("#{'${res.candidate}'.split(',')}")
	private List<String> givenList;

	@Autowired
	private ResourceLoader resourceLoader;

	@Override
	public void write(String currentName) throws IOException, ParseException {
		final JSONParser jsonParser = new JSONParser();
		//final File file = ResourceUtils.getFile("classpath:db.json");
		final Resource resource = resourceLoader.getResource("/app/db.json");
		final File file = resource.getFile();
		JSONObject coreData = null;
		try (final FileReader reader = new FileReader(file)) {
			// Read JSON file
			coreData = (JSONObject) jsonParser.parse(reader);
			final JSONArray candidateWithStatusList = (JSONArray) coreData.get("Candidate With Status");

			for (final Object candidateList : candidateWithStatusList) {
				final JSONObject candidates = (JSONObject) candidateList;
				final Set finalList = candidates.keySet();
				final Iterator<String> it = finalList.iterator();
				while (it.hasNext()) {
					final String key = it.next();
					if (key.equalsIgnoreCase(currentName)) {
						candidates.put(currentName, "1");
						break;
					}
				}
			}

		}

		try (FileWriter a = new FileWriter(file)) {
			a.write(coreData.toJSONString());
		}

	}

	@Override
	public Map<String, String> read() throws FileNotFoundException, IOException, ParseException {
		final Map<String, String> candiatesMap = new HashMap<String, String>();
		final JSONParser jsonParser = new JSONParser();
		//final File file = ResourceUtils.("classpath:db.json");
		//final File file = new File("./app/db.json");
		final Resource resource = resourceLoader.getResource("./app/db.json");
		final File file = resource.getFile();
		try (final FileReader reader = new FileReader(file)) {
			// Read JSON file
			final JSONObject coreData = (JSONObject) jsonParser.parse(reader);
			final JSONArray candidateWithStatusList = (JSONArray) coreData.get("Candidate With Status");

			for (final Object candidateList : candidateWithStatusList) {
				final JSONObject candidates = (JSONObject) candidateList;
				final Set finalList = candidates.keySet();
				final Iterator<String> it = finalList.iterator();
				while (it.hasNext()) {
					final String key = it.next();
					candiatesMap.put(key, (String) candidates.get(key));
				}
			}
		}
		return candiatesMap;
	}

	@Override
	public void reset() throws IOException {

		final JSONObject obj = new JSONObject();
		obj.put("Name", "Pick a name datacentre with status");
		obj.put("Author", "Jeevan Abraham");

		final JSONArray candidate = new JSONArray();
		final JSONObject obj2 = new JSONObject();

		for (final String names : givenList) {
			obj2.put(names, "0");
		}

		candidate.add(obj2);
		obj.put("Candidate With Status", candidate);
		//final File file = ResourceUtils.getFile("classpath:db.json");
		final Resource resource = resourceLoader.getResource("classpath:db.json");
		final File file = resource.getFile();
		try (FileWriter a = new FileWriter(file)) {
			a.write(obj.toJSONString());
		}

	}
}
