package com.pickaname.serv.impl;

import java.net.URL;
import java.net.URLClassLoader;

public class Test{

	public static void main (String args[]) {

		final ClassLoader cl = ClassLoader.getSystemClassLoader();

		final URL[] urls = ((URLClassLoader)cl).getURLs();

		for(final URL url: urls){
			System.out.println(url.getFile());
		}

	}
}