@ECHO OFF	
ECHO ============================
ECHO Stoping current container if exist
ECHO ============================
docker stop pick-x
ECHO ============================
ECHO Removing current container if exist
ECHO ============================
docker rm pick-x
ECHO ============================
ECHO Removing current container image if exist
ECHO ============================
docker rmi pick-x
ECHO ============================
ECHO Creating new Jar File
ECHO ============================
call mvn package
docker rmi pick-x
ECHO ============================
ECHO Creating new Image
ECHO ============================
docker build -f Dockerfile -t pick-x .
ECHO ============================
ECHO Spinning up the container
ECHO ============================
docker run -d --name pick-x -p 1111:7777 pick-x
PAUSE